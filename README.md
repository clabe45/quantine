## Installation
Instructions for running the backend api locally or installing the mobile app can be found [here](https://gitlab.com/clabe45/quantine/-/blob/master/INSTALLING.md)
## Inspiration
As we can see around the world, many supermarkets are emptied because people are panic buying. Panic buying is a type of herd behavior, most common at disasters such as the COVID-19 pandemic. Our idea is to help people buy only what they need so they don't waste money and don't cause any shortages of essential items and being more organized.
## What it does
Users can have inventories, which contain a set of items, added by the user. When someone creates a new inventory, he/she becomes the owner and has the ability to invite and kick others.When someone joins the owner's inventory, he/she becomes a member. Each user can create up to 2 inventories (to be the owner) and join up to 3 other inventories after getting invitation from other users (to be a member). The member can leave the inventory. Each item has type. The type can be either food and drinks or household products. The item also has the option to upload pictures and what amount of the items is in stock, as well as montly consumption amount and its history of the last months.
## How we built it
We decided to make a mobile app that involves using React Native. As we are new to React Native, we used expo cli to make the things quickly. We used MongoDb for building the database, as well as mongoose js for object data modeling. The backend server, containing the REST api and all the bussiness logic is built with node js and express.
## Challenges We ran into
### Timezone Challenges

### Implementation
At the frontend, we had some confusion on whether we should use class components or hooks but eventually we settled for hooks as there was a lot of useful hooks that we could use and the code is cleaner in general. We also had some cross platform issues with Android and iOS, with a lot of crashing in Android, and we fixed that by specifying the project to detect all the javascript and typescript files in app.json

## What we learned
* We learned to use Expo and React Native as well as integrating both frontend and backend. 
* Dealing with inventory management
* Node js, express and mongoose js
## What's next for Quantine
* Multi-language support
* Security fixes
* New items scanning from scanning receipts  
* New item types
* "Remember login" ability for better user experience, 
* Weekly consumption amount history, montly/weekly consumption predictions
* Reminders for things like expiration dates
* Labels for organizing items, list of products to buy and print it, "buy this product" option
* Testing and more