import React, { useReducer, useMemo, useEffect } from 'react'
import { AsyncStorage } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import PropTypes from 'prop-types'

import Profile from './views/Profile'
import EditProfile from './views/EditProfile'
import Login from './views/Login'
import SignUp from './views/SignUp'
import Inventory from './views/Inventory'

import AuthContext from './context/AuthContext'

import { SIGN_IN_URL, SIGN_UP_URL } from 'react-native-dotenv'
import axios from 'axios'

const AuthStack = createStackNavigator()
const ProfileStack = createStackNavigator()
const Tab = createBottomTabNavigator()

App.propTypes = {
  focused: PropTypes.object
}

export default function App () {
  const [state, dispatch] = useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            userToken: action.token,
            isLoading: false
          }
        case 'SIGN_IN':
          return {
            ...prevState,
            isSignOut: false,
            userToken: action.token
          }
        case 'SIGN_OUT':
          return {
            ...prevState,
            isSignout: true,
            userToken: null
          }
        default:
          return {
            ...prevState
          }
      }
    },
    {
      isLoading: true,
      isSignOut: false,
      userToken: null
    }
  )

  useEffect(() => {
    let mounted = true

    const bootstrapAsync = async () => {
      let userToken

      try {
        userToken = await AsyncStorage.getItem('userToken')
      } catch (e) {
        console.log('Error: ' + e)
      }

      if (mounted) {
        dispatch({ type: 'RESTORE_TOKEN', token: userToken })
      }
    }

    bootstrapAsync()
    return () => mounted = false
  }, [])

  const authContext = useMemo(
    () => ({
      signIn: async data => {
        await axios.post(SIGN_IN_URL, {
          email: data.userEmail,
          password: data.password
        })
        .then(response => {
          if (response.data.token) {
            AsyncStorage.setItem('userToken', response.data.token)
            AsyncStorage.setItem('fullName', response.data.fullName)
            dispatch({ type: 'SIGN_IN', token: response.data.token })
          }
        }).catch(error => {
          if(error.message = "Request failed with status code 400") {
            alert("Invalid Email or Password")
          } else {
            alert(error.message)
          }
        })
      },
      signOut: () => {
        AsyncStorage.clear()
        dispatch({ type: 'SIGN_OUT' })
      },
      signUp: async data => {
        await axios.post(SIGN_UP_URL, {
          email: data.userEmail,
          fullName: data.userPassword,
          password: data.fullName,
          profilePicUrl: ''
        })
        .then(response => {
          if (response.data.token) {
            AsyncStorage.setItem('userToken', response.data.token)
            AsyncStorage.setItem('fullName', response.data.fullName)
            dispatch({ type: 'SIGN_IN', token: response.data.token })
          }
        }).catch(error => {
          if(error.message = "Request failed with status code 400") {
            alert("Invalid Email or Password")
          } else {
            alert(error.message)
          }       
        })
      }
    }), []
  )

  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        {state.userToken == null ? (
          <AuthStack.Navigator initialRouteName="LoginScreen" headerMode="none">
            <AuthStack.Screen
              name="LoginScreen"
              component={Login} />
            <AuthStack.Screen
              name="SignUpScreen"
              component={SignUp}
            />
          </AuthStack.Navigator>
        ) : (
          <Tab.Navigator
            initialRouteName="Inventory"
            screenOptions={({ route }) => ({
              tabBarIcon: ({ focused }) => {
                let iconName

                if (route.name === 'Inventory') {
                  iconName = focused
                    ? 'cube'
                    : 'cube-outline'
                } else if (route.name === 'Profile') {
                  iconName = focused ? 'account' : 'account-outline'
                }

                return <MaterialCommunityIcons name={iconName} size={24}/>
              }
            })}
            tabBarOptions={{
              showIcon: true,
              showLabel: false
            }}>
            <Tab.Screen name="Inventory">
                {() => <Inventory/>}
            </Tab.Screen>
            <Tab.Screen name="Profile">
              {() => (
                <ProfileStack.Navigator initialRouteName="ProfileScreen" headerMode="none">
                  <ProfileStack.Screen
                    name="ProfileScreen"
                    component={Profile}
                  />
                  <ProfileStack.Screen
                    name="EditProfileScreen"
                    component={EditProfile}
                  />
                </ProfileStack.Navigator>
              )}
            </Tab.Screen>
          </Tab.Navigator>
        )}
      </NavigationContainer>
    </AuthContext.Provider>
  )
}
