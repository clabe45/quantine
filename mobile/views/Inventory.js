import axios from 'axios'
import React, { Component } from 'react'
import { View, StyleSheet, Dimensions, TouchableHighlight, TouchableOpacity, Modal, Picker } from 'react-native'
import { Text, Button, Card, Input } from 'react-native-elements'
import { Feather, Entypo } from '@expo/vector-icons'
import PropTypes from 'prop-types'

import * as Font from 'expo-font'

import styles from '../styles/styles'

const dummyUser = 'admin@admin.com'

/*
 * TODO:
 * - Center items' content vertically
 * - Make all items equal height
 * - Center modal dialogs vertically
 */

// Get screen width and height
const screenWidth = Math.round(Dimensions.get('window').width)
const screenHeight = Math.round(Dimensions.get('window').height)

const inventoryStyles = StyleSheet.create({
  noInventoryMessage: {
    marginTop: 0.01 * screenHeight,
    fontSize: 16,
    color: 'grey'
  },
  titleBar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 0.01 * screenHeight
  },
  inventoryPicker: {
    flex: 5
  },
  title: {
    fontFamily: 'open-sans-light',
    fontWeight: 'normal',
    flex: 1
  },
  card: {
    minHeight: 0.1 * screenHeight,
    backgroundColor: '#0000'
  },
  itemWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  name: {
    fontSize: 20,
    fontFamily: 'open-sans-light',
    color: 'grey'
  },
  amount: {
    fontSize: 16,
    fontFamily: 'open-sans-light',
    color: 'grey'
  },
  restockButton: {
    paddingHorizontal: 10,
    paddingVertical: 10,
    backgroundColor: '#c21'
  },
  restockTitle: {
    fontFamily: 'open-sans-light',
    fontSize: 18
  },
  modalTint: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: '#0000'
  },
  modalWrapper: {
    justifyContent: 'center'
  },
  modal: {
    marginHorizontal: 0.1 * screenWidth,
    marginVertical: 0.25 * screenHeight,
    padding: 0.02 * screenHeight,
    backgroundColor: 'white',
    shadowColor: 'black',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.5,
    shadowRadius: 4,
    elevation: 5
  },
  modalTitleBar: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  modalTitle: {
    fontSize: 18
  },
  modalContent: {
    marginHorizontal: 0.01 * screenWidth,
    marginTop: 0.01 * screenHeight,
    marginBottom: 0
  },
  modalSubmitWrapper: {
    alignItems: 'flex-end'
  },
  modalSubmitEditWrapper: {
    alignItems: 'flex-end',
    flexDirection: 'row',
    justifyContent: 'space-evenly'
  },
  modalSubmitButton: {
    marginTop: 0.02 * screenHeight,
    paddingHorizontal: 20,
    paddingVertical: 6
  }
})

class Inventory extends Component {
  static propTypes = {}

  constructor (props) {
    super(props)

    this.state = {
      fontLoaded: false,
      inventories: [],
      inventory: null,
      inventoryItems: [],
      newInventoryModalVisible: false,
      newInventoryName: null,
      restockModalVisible: false,
      restockingItem: null,
      restockingItemConsumed: {amount: 0},
      restockingItemConsumptionHistory: [],
      restockingItemNewAmount: null,
      addModalVisible: false,
      editModalVisible: false,
      editingItemNewName: null,
      editingItemNewAmount: null
    }
	}
	
	async loadFont () {
		// Load custom fonts on mount
		await Font.loadAsync({
			'open-sans-light': require('../assets/fonts/OpenSans-Light.ttf'),
			'open-sans-regular': require('../assets/fonts/OpenSans-Regular.ttf')
		})
		this.setState({
			fontLoaded: true
		})
	}

	async getInventories () {
    return new Promise(async (resolve, reject) => {
      const { data } = await axios.get('https://quantine.herokuapp.com/api/inventories')
      const state = { inventories: data }
      if (state.inventories.length)
        state.inventory = state.inventories[0]
      this.setState(state)
      resolve()
    })
  }

  getItems () {
    return new Promise(async resolve => {
      const { data } = await axios.get('https://quantine.herokuapp.com/api/items')
      const inInventory = data.filter(item => item.inventory._id === this.state.inventory._id)
      this.setState({ inventoryItems: inInventory })
      resolve()
    })
  }

  async componentDidMount () {
    this.loadFont()
    await this.getInventories()
  }

  closeRestockModal = () => {
    this.setState({
      restockModalVisible: false,
      restockingItemName: null,
      restockingItemNewAmount: null,
      restockingItemConsumed: { amount: 0}
    })
  }

  onChangeInventory = id => {
    let inventory
    if (id === -1) {
      this.setState({ newInventoryModalVisible: true })
      inventory = null
    } else {
      inventory = this.state.inventories.find(inv => inv._id === id)
    }
    this.setState({ inventory }, this.getItems)
  }

  onAddInventory = async () => {
    const name = this.state.newInventoryName
    if (!name) {
      alert('Please enter a name')
      return
    }

    await axios.post('https://quantine.herokuapp.com/api/inventories', {
      name, owner: dummyUser
    })
    await this.getInventories()
    this.setState({
      newInventoryModalVisible: false,
      newInventoryName: null
    })
  }

  onAddInventory = async () => {
    const name = this.state.newInventoryName
    if (!name) {
      alert('Please enter a name')
      return
    }

    await axios.post('https://quantine.herokuapp.com/api/inventories', {
      name, owner: dummyUser
    })
    await this.getInventories()
    this.setState({
      newInventoryModalVisible: false,
      newInventoryName: null
    })
  }

  onOpenRestock = item => {
    this.setState({
      restockModalVisible: true,
      restockingItem: item,
      restockingItemNewAmount: item.amount,
      restockingItemConsumptionHistory: item.consumption || []
    })
  }

  onSubmitRestock = async () => {
    const amount = this.state.restockingItemNewAmount
    const consumption  = this.state.restockingItemConsumed
    let restockObj = { amount, consumption}
    if (isNaN(amount)) {
      delete restockObj.amount
    }
    if(consumption === undefined || isNaN(consumption.amount) || consumption.amount === 0) delete restockObj.consumption
    //if the req is empty, close the modal
    if(Object.keys(restockObj).length === 1 && restockObj.constructor === Object) {
      this.closeRestockModal()
      return
    }
    // console.log({ ...restockObj })
    await axios.put(`https://quantine.herokuapp.com/api/items/${this.state.restockingItem._id}`, { ...restockObj })
    await this.getItems()
    this.closeRestockModal()
  }

  onOpenAdd = () => {
    this.setState({
      addModalVisible: true
    })
  }

  onSubmitAdd = async () => {
    const name = this.state.addingItemName
    const amount = parseInt(this.state.addingItemAmount)
    if (!name) {
      alert('Please enter a name')
      return
    }
    if (isNaN(amount)) {
      alert('Please enter a valid amount')
      return
    }

    await axios.post('https://quantine.herokuapp.com/api/items', {
      name, amount, inventory: this.state.inventory._id
    })

    this.setState({
      addModalVisible: false,
      addingItemName: null,
      addingItemAmount: null
    })
    this.getItems()
  }

  onEditOpen = item => {
    this.setState({
      editModalVisible: true,
      editingItem: item,
      editingItemNewName: item.name,
      editingItemNewAmount: item.amount
    })
  }

  onSubmitEdit = async () => {
    const name = this.state.editingItemNewName
    const amount = parseInt(this.state.editingItemNewAmount)
    if (!name) {
      alert('Please enter a name')
      return
    }
    if (isNaN(amount)) {
      alert('Please enter a valid amount')
      return
    }

    await axios.put(`https://quantine.herokuapp.com/api/items/${this.state.editingItem._id}`, { name, amount })
    await this.getItems()
    this.setState({
      editModalVisible: false,
      editingItem: null,
      editingItemNewName: null,
      editingItemNewAmount: null
    })
  }

  onDelete = async () => {
    const item = this.state.editingItem
    await axios.delete(`https://quantine.herokuapp.com/api/items/${item._id}`)
    await this.getItems()
    this.setState({
      editModalVisible: false,
      editingItem: null,
      editingItemNewName: null,
      editingItemNewAmount: null
    })
  }

  renderNewInventoryModal = () => {
    return (
      <Modal
        transparent={true}
        visible={this.state.newInventoryModalVisible}
      >
        <View style={inventoryStyles.modalTint}/>
        <View style={inventoryStyles.modalWrapper}>
          <View style={inventoryStyles.modal}>
            <View style={inventoryStyles.modalTitleBar}>
              <Text style={inventoryStyles.modalTitle}>Create Inventory</Text>
              <TouchableHighlight
                onPress={() => this.setState({ newInventoryModalVisible: false })}
              >
                <Entypo name='cross' size={24} />
              </TouchableHighlight>
            </View>
            <View style={inventoryStyles.modalContent}>
              <Input
                placeholder="Groceries"
                value={this.state.newInventoryName ? `${this.state.newInventoryName}` : ''}
                onChangeText={text => this.setState({ newInventoryName: text })}
                onSubmitEditing={this.onAddInventory}
              />
              <View style={inventoryStyles.modalSubmitWrapper}>
                <Button
                  buttonStyle={inventoryStyles.modalSubmitButton}
                  title="Create"
                  onPress={this.onAddInventory}/>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    )
  }

  renderRestockModal = () => {
    const ConS = this.state.restockingItemConsumptionHistory
    const i = ConS.length - 1;
    return (
      <Modal
        transparent={true}
        visible={this.state.restockModalVisible}
      >
        <View style={inventoryStyles.modalTint}/>
        <View style={inventoryStyles.modalWrapper}>
          <View style={inventoryStyles.modal}>
            <View style={inventoryStyles.modalTitleBar}>
              <Text style={inventoryStyles.modalTitle}>Stock {this.restockModalVisible ? this.state.restockingItem.name : null}</Text>
              <TouchableHighlight
                onPress={() => this.setState({ restockModalVisible: false, restockingItem: null })}
              >
                <Entypo name='cross' size={24} />
              </TouchableHighlight>
            </View>
            <View style={inventoryStyles.modalContent}>
            <Text style={{ color: 'black', fontSize: 20, marginBottom: 15, fontFamily: 'open-sans-light' }}>{
              ConS.length !== 0 ? (`Last consumed ${ConS[i].amount} at ${new Date(ConS[i].Date)}`) : 'No consumption history'}
              </Text>
              <Text>New usage</Text>
              <Input 
                value={this.state.restockingItemConsumed.amount ? this.state.restockingItemConsumed.amount : ''} 
                onChangeText={text => this.setState({ restockingItemConsumed: { amount: isNaN(parseInt(text)) ? '' : parseInt(text) }})} />
              <Text>Restock</Text>
              <Input
                placeholder="0"
                value={this.state.restockingItemNewAmount ? `${this.state.restockingItemNewAmount}` : ''}
                onChangeText={text => this.setState({ restockingItemNewAmount: isNaN(parseInt(text)) ? '' : parseInt(text) })}
                onSubmitEditing={this.onSubmitRestock}
              />
              <View style={inventoryStyles.modalSubmitWrapper}>
                <Button
                  buttonStyle={inventoryStyles.modalSubmitButton}
                  title="Restock"
                  onPress={this.onSubmitRestock}/>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    )
  }

  renderAddModal = () => {
    return (
      <Modal
        transparent={true}
        visible={this.state.addModalVisible}
      >
        <View style={inventoryStyles.modalTint}/>
        <View style={inventoryStyles.modalWrapper}>
          <View style={inventoryStyles.modal}>
            <View style={inventoryStyles.modalTitleBar}>
              <Text style={inventoryStyles.modalTitle}>Add item</Text>
              <TouchableHighlight
                onPress={() => this.setState({ addModalVisible: false, addingItemName: null, addingItemAmount: null })}
              >
                <Entypo name='cross' size={24} />
              </TouchableHighlight>
            </View>
            <View style={inventoryStyles.modalContent}>
              <Input
                placeholder="Eggs"
                value={this.state.addingItemName}
                onChangeText={text => this.setState({ addingItemName: text })}
              />
              <Input
                placeholder="1"
                value={this.state.addingItemAmount ? `${this.state.addingItemAmount}` : ''}
                onChangeText={text => this.setState({ addingItemAmount: isNaN(parseInt(text)) ? '' : parseInt(text) })}
                onSubmitEditing={this.onSubmitAdd}
              />
              <View style={inventoryStyles.modalSubmitWrapper}>
                <Button
                  buttonStyle={inventoryStyles.modalSubmitButton}
                  title="Add"
                  onPress={this.onSubmitAdd}/>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    )
  }

  renderEditModal = () => {
    return (
      <Modal
        transparent={true}
        visible={this.state.editModalVisible}
      >
        <View style={inventoryStyles.modalTint}/>
        <View style={inventoryStyles.modalWrapper}>
          <View style={inventoryStyles.modal}>
            <View style={inventoryStyles.modalTitleBar}>
              <Text style={inventoryStyles.modalTitle}>Edit item</Text>
              <TouchableHighlight
                onPress={() => this.setState({ editModalVisible: false, editingItem: null, editingItemNewName: null, editingItemNewAmount: null })}
              >
                <Entypo name='cross' size={24} />
              </TouchableHighlight>
            </View>
            <View style={inventoryStyles.modalContent}>
              <Input
                placeholder="Eggs"
                value={this.state.editingItemNewName}
                onChangeText={text => this.setState({ editingItemNewName: text })}
              />
              <Input
                placeholder="1"
                value={this.state.editingItemNewAmount ? `${this.state.editingItemNewAmount}` : ''}
                onChangeText={text => this.setState({ editingItemNewAmount: isNaN(parseInt(text)) ? '' : parseInt(text) })}
                onSubmitEditing={this.onSubmitEdit}
              />
              <View style={inventoryStyles.modalSubmitEditWrapper}>
                <Button
                  buttonStyle={inventoryStyles.modalSubmitButton}
                  title="Save"
                  onPress={this.onSubmitEdit}/>
                <Button
                  title="Delete"
                  buttonStyle={{
                    marginTop: 0.02 * screenHeight,
                    paddingHorizontal: 20,
                    paddingVertical: 6,
                    backgroundColor: '#c21'
                  }}
                  onPress={this.onDelete}/>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    )
  }

  render () {
    return this.state.fontLoaded ? (
      <View style={styles.container}>
        {this.renderNewInventoryModal()}
        {this.renderRestockModal()}
        {this.renderAddModal()}
        {this.renderEditModal()}
        <View style={inventoryStyles.titleBar}>
          {
            this.state.inventories.length
              ? (
                <Picker
                  style={inventoryStyles.inventoryPicker}
                  onValueChange={this.onChangeInventory}
                  selectedValue={this.state.inventory ? this.state.inventory._id : -1}>
                  {
                    this.state.inventories.map((inventory, i) => (
                      <Picker.Item key={i} label={inventory.name} value={inventory._id}/>
                    ))
                  }
                  <Picker.Item key={-1} label="New inventory..." value={-1}/>
                </Picker>
              ) : (
                <Button type="clear" title="Add inventory" onPress={() => this.setState({ newInventoryModalVisible: true })}/>
              )
          }
          {
            this.state.inventory ? (
              <Button
                type="clear"
                icon={
                  <Feather name="plus" size={0.065 * screenHeight} />}
                onPress={this.onOpenAdd}/>
            ) : null
          }
        </View>
        {
          this.state.inventory
            ? (
              <View>
                {
                  this.state.inventoryItems.map((item, i) => (
                    <TouchableOpacity
                      key={i}
                      onPress={() => this.onEditOpen(item)}>
                      <Card containerStyle={inventoryStyles.card}>
                        <View style={inventoryStyles.itemWrapper}>
                          <Text style={inventoryStyles.name}>{item.name}</Text>
                          {
                            item.amount > 0
                              ? (
                                <TouchableHighlight
                                  onPress={() => this.onOpenRestock(item)}
                                >
                                  <Text style={inventoryStyles.amount}>Amount: {item.amount}</Text>
                                </TouchableHighlight>
                              ) : (
                                <Button
                                  title="Restock"
                                  onPress={() => this.onOpenRestock(item)}
                                  buttonStyle={inventoryStyles.restockButton}
                                  titleStyle={inventoryStyles.restockTitle}
                                />
                              )
                          }
                        </View>
                      </Card>
                    </TouchableOpacity>
                  ))
                }
              </View>
            ) : <Text style={inventoryStyles.noInventoryMessage}>Please select an inventory</Text>
        }
      </View>
    ) : (
      <View/> // must not be null (for the initial screen)
    )
  }
}

export default Inventory
