const Inventory = require('../models/inventory');
const Item = require('../models/item');
const {user} = require('../core/user');

const createInventoryMethods = (InventoryModel, ItemModel) => {
  return {
    /**
     * Find many Inventories
     */
    async findMany () {
      const inventories = await InventoryModel.find({}).populate('owner');
      return inventories;
    },
    /**
     * Find one Inventory
     * @param {*} search search obj
     */
    async findOne (search = {}) {
      const inventory = InventoryModel.findOne(search);
      return inventory;
    }, 
    /**
     * Create new inventory
     * @todo limit: can't create if already owns 2 Inventories
     */
    async createOne (inventoryObj) {
      try {
        let ownerId = inventoryObj.owner;
        if (inventoryObj.owner.includes('@')) {
          const owner = await user.findOne({ email: inventoryObj.owner } )
          ownerId = owner._id;
        }
        const inventory = await InventoryModel.create({ ...inventoryObj, owner: ownerId });
        const foundInventory = await InventoryModel.findById(inventory._id);
        return foundInventory;
      } catch(err){
        return err;
      }
    },
    /**
     * Get all items
     */
    async getItems () {
      const items = await ItemModel.find({}).populate('inventory');
      return items;
    },
    async getOneItem (search = {}) {
      const items = await ItemModel.findOne(search).populate('inventory');
      return items;
    },
    /**
     * Creates new item
     * @param {*} itemObj create object
     */
    async createItem (itemObj) {
      const item = await ItemModel.create({ ...itemObj });
      const inventory = await InventoryModel.findById(item.inventory);
      inventory.items.push(item._id);
      await inventory.save();
      const foundItem = await ItemModel.findById(item._id);
      return foundItem;
    },
    async updateItem (id, itemObj) {
      const consumption = itemObj.consumption;
      delete itemObj.consumption;
      const item = await ItemModel.findByIdAndUpdate(id, itemObj, { runValidators: true });
      
      if(itemObj.amount !== null && itemObj.amount !== undefined) {
        item.amount += itemObj.amount;
        await item.save();
      }
      if(consumption !== null && consumption !== undefined) {
        item.amount -= consumption.amount;
        item.consumption.push({ Date: new Date(), amount: consumption.amount });
        await item.save();
      }
      const foundItem = await ItemModel.findById(item._id);
      return foundItem;
    },
    async removeItem (id) {
      const item = await ItemModel.findByIdAndDelete(id);
      return item;
    }
  };
};

const inventory = createInventoryMethods(Inventory, Item);

module.exports = { createInventoryMethods, inventory };
