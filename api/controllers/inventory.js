const { inventory } = require('../core/inventory');

const createInventoryMethods = () => {
  return {
    async getOne (req, res, next){
      try {
        const Inventory = await inventory.findOne(req.params);
        return res.json(Inventory);
      } catch (err) {
        return next(err);
      }
    },
    async getMany(req, res, next){
      try {
        const Inventory = await inventory.findMany();
        return res.json(Inventory);
      } catch (err) {
        return next(err);
      }
    },
    async new(req, res, next) {
      try {
        const Inventory = await inventory.createOne(req.body);
        return res.json(Inventory);
      } catch (err) {
        return next(err);
      }
    },
    async getItems(req, res, next) {
      try {
        const Items = await inventory.getItems();
        return res.json(Items);
      } catch (err) {
        return next(err);
      }
    },
    async getItem(req, res, next) {
      try {
        const idObj = { _id: req.params.id }
        const Item = await inventory.getOneItem(idObj);
        return res.json(Item);
      } catch (err) {
        return next(err);
      }
    },
    async editItem(req, res, next) {
      try {
        const id = req.params.id;
        const Item = await inventory.updateItem(id, req.body);
        return res.json(Item);
      } catch (err) {
        next(err);
      }
    },
    async newItem(req, res, next) {
      try {
        const Item = await inventory.createItem(req.body);
        return res.json(Item);
      } catch (err) {
        return next(err);
      }
    },
    async deleteItem(req, res, next) {
      try {
        const id = req.params.id;
        const Item = await inventory.removeItem(id);
        return res.json(Item);
      } catch (err) {
        return next(err);
      }
    }
  };
};

const Inventory = createInventoryMethods();
module.exports = { Inventory, createInventoryMethods };