const express = require('express');
const apiRouter = express.Router();
const { auth } = require('../controllers/auth');
const startMongooseConnection = require('../config/mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const errorHandler = require('../controllers/errorHandler');
const inventoryRouter = require('./inventoryRouter');
const {Inventory} = require('../controllers/inventory');

startMongooseConnection();

apiRouter.use(cors());
apiRouter.use(bodyParser.json());

apiRouter.post('/auth/signin', auth.signin);
apiRouter.post('/auth/signup', auth.signup);

apiRouter.use('/inventories', inventoryRouter);

apiRouter.route('/items')
.get(Inventory.getItems)
.post(Inventory.newItem);

apiRouter.route('/items/:id')
.get(Inventory.getItem)
.put(Inventory.editItem)
.delete(Inventory.deleteItem);

apiRouter.use((req, res, next) => {
  next({ status: 404, message: 'Not Found' });
});

apiRouter.use(errorHandler);

module.exports = apiRouter;
