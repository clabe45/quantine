const express = require('express');
const inventoryRouter = express.Router();
const { Inventory } = require('../controllers/inventory');

inventoryRouter.route('/')
.get(Inventory.getMany)
.post(Inventory.new);

inventoryRouter.route('/:_id')
.get(Inventory.getOne);

module.exports = inventoryRouter;
