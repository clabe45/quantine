const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const consumptionSchema = Schema({
  Date: { type: Date },
  amount: { type: Number} 
});

const itemSchema = Schema({
  // _id is added by default field in mongoose
  name: {
    type: String,
    required: true
  },
  itemType: { type: String, enum: ['Food and drinks', 'Household product']},
  inventory: {
    type: Schema.Types.ObjectId, ref: 'Inventory'
  },
  consumption: [consumptionSchema],
  //how much is in stock
  amount: {
    type: Number,
    required: false,
    unique: false
  }
});


const Item = mongoose.model('Item', itemSchema);

module.exports = Item;
