const mongoose = require('mongoose');
const Schema = mongoose.Schema;



const inventorySchema = Schema({
  // _id is added by default field in mongoose
  name: { type: String, required: true },
  owner: { type: Schema.Types.ObjectId, ref: 'User', required: true },
  members: [{type: Schema.Types.ObjectId, ref: 'User'}],
  items: [{ type: Schema.Types.ObjectId, ref: 'Item' }]
});

inventorySchema.pre('save', async function (next) {
  try {
    next();
  } catch (err) {
    return next(err);
  }
});

const Inventory = mongoose.model('Inventory', inventorySchema);

module.exports = Inventory;
