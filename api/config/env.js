const variables = {
  NODE_ENV: process.env.NODE_ENV || 'development', //eslint-disable-line
  dbUri: process.env.dbUri || "mongodb://localhost/quantine", //eslint-disable-line
  PORT: process.env.PORT || 8081, //eslint-disable-line
  JWT_SECRET: process.env.JWT_SECRET || 'THIS SECRET IS NOT FOR PRODUCTION' //eslint-disable-line
};

if(variables.NODE_ENV === 'development') {
  variables.dbUri = 'mongodb://localhost/quantine';
}
module.exports = { ...variables };
