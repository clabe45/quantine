# Quantine Installation Guide
## Installing the mobile app
### Android install from APK file
Open up our shared google drive folder (the root directory is Quantine) [here](https://drive.google.com/drive/folders/1DvuGPEyFqAY3mxFa1kSj2hS_OTCkMous?usp=sharing) and follow the instructions below:
#### Install our last build
Navigate to /"Android app", download the APK file at the root of the directory and install it.
#### Install older builds
Navigate to /"Android app"/older, choose a APK file and install it.
### IOS
TODO
## Running the api locally
[api/README.md](https://gitlab.com/clabe45/quantine/-/tree/master/api)